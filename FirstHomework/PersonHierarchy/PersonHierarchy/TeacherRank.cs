﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonHierarchy
{
    public enum TeacherRank
    {
        NONE,
        RANK2,
        RANK1,
        DOCTORATE
    }
}

